## Laravel Take Home Exercise

To get a feeling of how you work, think, and structure your projects, we've come up with this small challenge which very closely resembles what you would encounter as a FullStack or Backend developer at Procuros.

You have 3h to complete this task but it's not a big issue if you don't finish.

## Setup
Please start with a fresh Laravel 9.x install.
Setup your environment however you feel most comfortable (Sail, Homestead, Valet, etc.).

The fastest way to kickstart a fresh project might be to install Docker Desktop and use Laravel Sail by running the following commands:
```bash
# Move to the directory you just cloned
cd trade-network-[SOME-STRING-HERE]

# Make sure you have Docker Desktop running
# Then run:
curl -s "https://laravel.build/example-app?with=mysql" | bash

# when completed:
cd example-app && ./vendor/bin/sail up -d
```
If you used the above, you should be able to:
- Access http://localhost
- Access the MySql database using:
  - Host: `127.0.0.1` or `localhost` (depends on your OS)
  - Port: `3306`
  - Username: `sail`
  - Password: `password`
  - Database: `example-app`
- Run Artisan commands using `./vendor/bin/sail php artisan COMMAND_HERE`
### Post-Setup
Please do an initial commit after you have setup your environment so we can more easily see the code you actually wrote in the following commits.
## The Task:
In a nutshell, Procuros is a business network that allows trade partners (companies) to exchange business documents (messages) with each other. Your task is to implement a simplified version of this network.

### General
- A **Trade Relationship** connects two **Trade Partners** (Companies) on the network
- **Trade Partners** can have one or more **Users** that can login to the network
- Keep models as simple as possible

### Features
- A page that allows Users to login using email+password (must)
- Everything except Login should only be accessible if authenticated (must)
- A page that displays the logged in User's name and the name of the Trade Partner they belong to
- A page that displays all Trade Partners that the currently logged in Trade Partner is connected to through a Trade Relationship (must)
- A page that allows the currently logged in Trade Partner to create new Trade Relationships with other Trade Partners (must)
- Above features are tested with Feature tests (nice to have)
- An API route to fetch all Trade Partners on the network (must)
- Trade Partners have API Tokens and the API route is only accessible with a valid API Token (nice to have)

### Notes
- HTML/CSS/JS are irrelevant, it does NOT matter how it looks like!

Best of luck 🤞
**Ben**
CTO - Procuros GmbH
